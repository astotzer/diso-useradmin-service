package ch.diso.useradmin.service;

import ch.diso.useradmin.api.GroupDto;
import ch.diso.useradmin.api.UserDto;
import ch.diso.useradmin.exception.UserNotFoundException;
import ch.diso.useradmin.model.Group;
import ch.diso.useradmin.model.GroupRepository;
import ch.diso.useradmin.model.User;
import ch.diso.useradmin.model.UserRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Component
@Transactional
public class UserAdminService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public UserDto getUser(String userId) {
        UserDto userDto = new UserDto();
        User user = loadUser(userId);
        BeanUtils.copyProperties(user, userDto);
        return userDto;
    }

    public UserDto createUser(UserDto userDto) {
        User user = new User();
        BeanUtils.copyProperties(userDto, user);
        user.setPassword(passwordEncoder.encode(userDto.getPassword()));
        user = userRepository.save(user);
        BeanUtils.copyProperties(user, userDto);
        return userDto;
    }

    public UserDto updateUser(String userId, UserDto userDto) {
        User user = new User();
        BeanUtils.copyProperties(userDto, user);
        user.setUserId(userId);
        user = userRepository.save(user);
        BeanUtils.copyProperties(user, userDto);
        return userDto;
    }

    public void deleteUser(String userId) {
        User user = loadUser(userId);
        userRepository.delete(user);
    }

    public List<GroupDto> getAllGroups() {
        List<GroupDto> result = new ArrayList<>();
        groupRepository.findAll().forEach(group -> {
            GroupDto groupDto = new GroupDto();
            BeanUtils.copyProperties(group, groupDto);
            result.add(groupDto);
        });
        return result;
    }

    public GroupDto createGroup(GroupDto groupDto) {
        Group group = new Group();
        BeanUtils.copyProperties(groupDto, group);
        group = groupRepository.save(group);
        BeanUtils.copyProperties(group, groupDto);
        return groupDto;
    }

    public String encodePassword(String password) {
        return passwordEncoder.encode(password);
    }

    private User loadUser(String userId) {
        return userRepository.findByUserId(userId).orElseThrow(
                () -> new UserNotFoundException(userId)
        );
    }

}
