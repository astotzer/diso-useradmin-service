package ch.diso.useradmin.boundary;

import ch.diso.useradmin.api.GroupDto;
import ch.diso.useradmin.api.UserDto;
import ch.diso.useradmin.service.UserAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/useradmin")
public class UserAdminController {

    @Autowired
    private UserAdminService userAdminService;

    @GetMapping("/user/{userId}")
    public UserDto getUser(@PathVariable("userId") String userId) {
        return userAdminService.getUser(userId);
    }

    @PostMapping("/user")
    public UserDto createUser(@RequestBody UserDto userDto) {
        return userAdminService.createUser(userDto);
    }

    @PutMapping("/user/{userId}")
    public UserDto updateUser(@PathVariable("userId") String userId, @RequestBody UserDto userDto) {
        return userAdminService.updateUser(userId, userDto);
    }

    @DeleteMapping("/user/{userId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteUser(@PathVariable("userId") String userId) {
        userAdminService.deleteUser(userId);
    }

    @GetMapping("/groups/all")
    public List<GroupDto> getAllGroups() {
        return userAdminService.getAllGroups();
    }

    @PostMapping("/group")
    public GroupDto createGroup(@RequestBody GroupDto groupDto) {
        return userAdminService.createGroup(groupDto);
    }

}
