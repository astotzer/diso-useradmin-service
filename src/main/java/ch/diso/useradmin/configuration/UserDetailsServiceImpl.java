package ch.diso.useradmin.configuration;

import ch.diso.useradmin.exception.UserNotFoundException;
import ch.diso.useradmin.model.User;
import ch.diso.useradmin.model.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String userId) throws UsernameNotFoundException {
        User user = userRepository.findByUserId(userId).orElseThrow(
                () -> new UserNotFoundException(userId)
        );
        if (null == user.getPassword()) {
            throw new UsernameNotFoundException("User " + userId + " not exists");
        }
        Set<GrantedAuthority> authorities = new HashSet<>();
        user.getGroups().forEach(role -> {
            authorities.add(new GrantedAuthorityImpl(role.getName()));
        });
        return new UserDetailsImpl(userId, user.getPassword(), authorities);
    }
}
