package ch.diso.useradmin.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import ch.diso.useradmin.api.GroupDto;
import ch.diso.useradmin.api.UserDto;
import ch.diso.useradmin.exception.UserNotFoundException;
import ch.diso.useradmin.model.Group;
import ch.diso.useradmin.model.GroupRepository;
import ch.diso.useradmin.model.User;
import ch.diso.useradmin.model.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RunWith(MockitoJUnitRunner.class)
public class UserAdminServiceTest {

    @InjectMocks
    private UserAdminService userAdminService;

    @Mock
    UserRepository userRepository;

    @Mock
    GroupRepository groupRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Test
    public void testGetUser() {
        User user = new User();
        user.setUserId("1234");
        user.setPrename("junit");
        user.setName("test");
        user.setId(Long.valueOf(1));
        when(userRepository.findByUserId("1234")).thenReturn(Optional.of(user));

        UserDto result = userAdminService.getUser("1234");

        assertEquals(result.getUserId(), "1234");
        assertEquals(result.getPrename(), "junit");
        assertEquals(result.getId(), Long.valueOf(1));
        verify(userRepository, times(1)).findByUserId("1234");
    }

    @Test(expected = UserNotFoundException.class)
    public void testGetUser_UserNotFound() {
        when(userRepository.findByUserId("1234")).thenReturn(Optional.empty());
        userAdminService.getUser("1234");
    }

    @Test
    public void testGetAllGroups() {
        List<Group> groups = new ArrayList<>();
        Group group1 = new Group();
        group1.setName("Group1");
        Group group2 = new Group();
        groups.add(group1);
        groups.add(group2);
        when(groupRepository.findAll()).thenReturn(groups);

        List<GroupDto> result = userAdminService.getAllGroups();

        assertEquals(result.size(), 2);
        verify(groupRepository, times(1)).findAll();
    }

    @Test
    public void testEncodePassword() {
        when(passwordEncoder.encode(anyString())).thenReturn("theencodedpassword");
        String result = userAdminService.encodePassword("blah");
        assertEquals(result, "theencodedpassword");
    }

    @Test
    public void testCreateUser() {
        Answer<User> answer = invocationOnMock -> {
            User user = invocationOnMock.getArgumentAt(0, User.class);
            user.setId(Long.valueOf(1));
            return user;
        };
        when(userRepository.save(any(User.class))).thenAnswer(answer);
        when(passwordEncoder.encode(anyString())).thenReturn("theencodedpassword");

        UserDto userDto = new UserDto();
        userDto.setName("junit");
        userDto.setPrename("test");
        userDto.setPassword("blah");

        userDto = userAdminService.createUser(userDto);

        assertEquals(userDto.getId(), Long.valueOf(1));
        assertEquals(userDto.getName(), "junit");
        assertEquals(userDto.getPrename(), "test");
        assertEquals(userDto.getPassword(), "theencodedpassword");

        verify(userRepository, times(1)).save(any(User.class));
    }

}
